import React, { Component } from 'react';
import './App.css';
import {library} from '@fortawesome/fontawesome-svg-core'
import {faLock, faUser, faSignInAlt, faAt, faUserPlus, faKey, faUndo, faSignOutAlt, faPlus, faPaw, faTag} from '@fortawesome/free-solid-svg-icons';

import {Login} from './components/Login/Login';
import {Signup} from './components/Signup/Signup';

import {Switch} from 'react-router-dom';
import {PrivateRoute} from './components/PrivateRoute';
import { PreLogRoute } from './components/PreLogRoute';
import {UserZoneSwitch} from './components/UserZoneSwitch';

library.add(faLock, faUser, faSignInAlt, faAt, faUserPlus, faKey, faUndo, faSignOutAlt, faPlus, faPaw, faTag);

class App extends Component {
  render() {
    return (
      <Switch>
        <PreLogRoute exact path="/login" component={Login} />
        <PreLogRoute exact path={"/signup"} component={Signup} />
        <PrivateRoute path={"/"} component={UserZoneSwitch} />
      </Switch>
    );
  }
}

export default App;