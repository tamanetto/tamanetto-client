import axios from 'axios';
import config from '../config';
import {setupCache} from 'axios-cache-adapter/src';

const headers = {
  "Content-Type": "application/json",
};

const cache = setupCache({
  maxAge: 15 * 60 * 1000,
});

const api = axios.create({
  baseURL: config.server,
  timeout: 1000,
  headers,
  adapter: cache.adapter,
});

// const burl = config.server;

function isAuth() {
  return (localStorage.getItem('token') !== null);
}

function logout() {
  localStorage.clear();
}

export default {
  login: (username, password) => {
    return api.post(`/user/login`, {
      username,
      password,
    }, {
      headers
    })
  },

  signup: (send) => {
    return api.post(`/user/signup`, send, {headers});
  },

  isAuth,

  userInfos: () => {
    if(!isAuth()) {
      return null;
    }

    return new Promise((resolve, reject) => {
      api.get(`/user`, {
        params: {
          token: localStorage.getItem('token'),
        }
      }, {
        headers
      }).then((data) => resolve(data.data)).catch((err) => {
        reject(err);
      });
    });
  },

  adopter: (name) => {
    if(!isAuth()) {
      return null;
    }

    return api.post(`/pet/create`, {
      token: localStorage.getItem('token'),
      name
    }, {headers});
  },

  logout,
};