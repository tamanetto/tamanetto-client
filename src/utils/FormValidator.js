import * as EmailValidator from 'email-validator';

export function validateMail(mailToValidate) {
  if(mailToValidate.length === 0) {
    return false;
  }
  return EmailValidator.validate(mailToValidate);
}

export function validateUsername(usernameToValidate) {
  return usernameToValidate.length >= 3 && usernameToValidate.length <= 255;
}

export function validatePassword(password) {
  return password.length >= 3;
}

export function validateCPassword(password, confirmation) {
  return password === confirmation;
}

export function validatePetName(petName) {
  return (petName.length >= 3 && petName.length <= 255);
}