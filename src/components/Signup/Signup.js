import React from 'react';
import {
  Button,
  InputGroup,
  Label,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroupText,
  InputGroupAddon,
  Row, ButtonGroup, FormFeedback, Alert, Container
} from 'reactstrap'
import API from '../../utils/API';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import * as FormValidator from '../../utils/FormValidator';

export class Signup extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
      cpassword: "",
      emailValide: 0,
      usernameValide: 0,
      passwordValide: 0,
      cpasswordValide: 0,
      alertVisible: false,
      alertMessage: '',
    };

    this.handleChange.bind(this);
    this.send.bind(this);
    this.testChange.bind(this);
    this.dismissAlert.bind(this);
    this.retourLogin.bind(this);
  }

  send = event => {
    event.preventDefault();
    if(this.state.username.length === 0)
    {
      this.setState({
        alertVisible: true,
        alertMessage: "Vous devez spécifier un nom d'utilisateur.",
        usernameValide: 2,
      })
    }

    if(this.state.email.length === 0)
    {
      this.setState({
        alertVisible: true,
        alertMessage: "Vous devez spécifier une adresse mail.",
        emailValide: 2,
      })
      return;
    }
    if(this.state.password.length === 0) {
      this.setState({
        alertVisible: true,
        alertMessage: "Vous devez spécifier un mot de passe.",
        passwordValide: 2,
      });
      return;
    }

    if(this.state.password !== this.state.cpassword) {
      this.setState({
        alertVisible: true,
        alertMessage: "Les mots de passe ne corrrespondent pas.",
        cpasswordValide: 2,
      });

      return;
    }

    const _send = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    };

    API.signup(_send).then((data) => {
      localStorage.setItem('token', data.data.token);
      // window.location = '/dashboard';
      this.props.history.push('/');
    }, (err) => {
      this.setState({
        alertVisible: true,
        alertMessage: err.response.data.text,
      });
      console.log(err);
      return;
    });
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });

    if(this.state[event.target.id + "Valide"] === 2) {
      this.testChange(event);
    }

    if(event.target.id === 'password') {
      if(this.state.cpasswordValide === 1 || this.state.cpasswordValide === 2)
      {
        this.setState({cpasswordValide: 0});
      }
    }
  }

  testChange = event => {
    let valide;
    switch(event.target.id) {
      case 'email':
        valide = FormValidator.validateMail(this.state.email);
        break;
      case 'username':
        valide = FormValidator.validateUsername(this.state.username);
        break;
      case 'password':
        valide = FormValidator.validatePassword(this.state.password);
        break;
      case 'cpassword':
        valide = FormValidator.validateCPassword(this.state.password, this.state.cpassword);
        break;
      default:
        break;
    }

    this.setState({[event.target.id + "Valide"]: (valide) ? 1 : 2});
  }

  dismissAlert = () => {
    this.setState({alertVisible: false});
  }

  retourLogin = () => {
    this.props.history.push('/login');
  }

  render() {
    return(
      <Container>
        <Row>
          <Col sm={12}>
            <Row>
              <Col sm={12}>
                <Alert color={"danger"} isOpen={this.state.alertVisible} toggle={this.dismissAlert}>
                  {this.state.alertMessage}
                </Alert>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <Form onSubmit={this.send}>
                  <FormGroup row>
                    <Label for="email" sm={2}>Adresse mail :</Label>
                    <Col sm={10}>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <FontAwesomeIcon icon='at'/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="email" name="email" id="email" placeholder="me@example.tld" valid={this.state.emailValide === 1} invalid={this.state.emailValide === 2} onChange={this.handleChange} value={this.state.email} onBlur={this.testChange} autoFocus noValidate />
                        <FormFeedback className={"text-right"}>L'adresse mail doit être valide.</FormFeedback>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="username" sm={2}>Nom d'utilisateur : </Label>
                    <Col sm={10}>
                      <InputGroup>
                        <InputGroupAddon addonType='prepend'>
                          <InputGroupText>
                            <FontAwesomeIcon icon='user'/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" name="username" id="username" placeholder="Nom d'utilisateur" valid={this.state.usernameValide === 1} invalid={this.state.usernameValide === 2} onBlur={this.testChange} onChange={this.handleChange} value={this.state.username} />
                        <FormFeedback className={"text-right"}>Le nom d'utilisateur doit comporter 3 à 255 caractères.</FormFeedback>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="password" sm={2}>Mot de passe : </Label>
                    <Col sm={10}>
                      <InputGroup>
                        <InputGroupAddon addonType={'prepend'}>
                          <InputGroupText>
                            <FontAwesomeIcon icon={'key'}/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type={"password"} name={'password'} id={'password'} placeholder={'Mot de passe'} onChange={this.handleChange} invalid={this.state.passwordValide === 2} valid={this.state.passwordValide === 1} onBlur={this.testChange} value={this.state.password} />
                        <FormFeedback className={'text-right'}>Votre mot de passe doit comporter 3 à 255 caractères.</FormFeedback>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for={'cpassword'} sm={2}>Confirmation : </Label>
                    <Col sm={10}>
                      <InputGroup>
                        <InputGroupAddon addonType={'prepend'}>
                          <InputGroupText>
                            <FontAwesomeIcon icon={'key'}/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type={'password'} name={'cpassword'} id={'cpassword'} placeholder={'Confirmation'} onChange={this.handleChange} onBlur={this.testChange} invalid={this.state.cpasswordValide === 2} valid={this.state.cpasswordValide === 1} value={this.state.cpassword} />
                        <FormFeedback className={'text-right'}>La confirmation doit être identique à votre mot de passe.</FormFeedback>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row className={'justify-content-center'}>
                    <ButtonGroup>
                      <Button color={'success'}><FontAwesomeIcon icon={'user-plus'}/> Valider</Button>
                      <Button color={'danger'}><FontAwesomeIcon icon={'undo'}/> Réinitialiser</Button>
                    </ButtonGroup>
                  </FormGroup>
                  <FormGroup row className={'justify-content-center'}>
                    <Button color={'warning'} onClick={this.retourLogin}><FontAwesomeIcon icon={'sign-in-alt'}/> Se connecter</Button>
                  </FormGroup>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }
}
