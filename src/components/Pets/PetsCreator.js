import React from 'react';
import {
  Alert, Button,
  ButtonGroup,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon, InputGroupText,
  Label,
  Row
} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as FormValidator from '../../utils/FormValidator';
import API from '../../utils/API'

export class PetsCreator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      nameValide: 0,
      alertVisible: false,
      alertMessage: '',
    };

    this.handleChange.bind(this);
    this.send.bind(this);
    this.dismissAlert.bind(this);
    this.testChange.bind(this);
    this.retourAccueil.bind(this);
  }

  handleChange = event => {
    this.setState({[event.target.id]: event.target.value});

    if(this.state[event.target.id + "Valide"] === 2) {
      this.testChange(event);
    }
  }

  send = event => {
    event.preventDefault();
    if(this.state.name.length < 3 || this.state.name.length > 255) {
      this.setState({alertVisible: true, alertMessage: "Le nom de l'animal est trop court."});
      return;
    }

    API.adopter(this.state.name).then((data) => this.props.history.push('/'))
      .catch(err => {
        if(err.response.status === 403) {
          API.logout();
          this.props.history.push('/');
        }
        this.setState({
          alertVisible: true,
          alertMessage: err.response.data.text,
        });
        console.log(err);
        return;
      })
  }

  testChange = event => {
    if(event.target.id === 'name') {
      const resultat = FormValidator.validatePetName(this.state[event.target.id]) ? 1 : 2;
      this.setState({
        [event.target.id + 'Valide']: resultat,
      });
    }
  }

  dismissAlert = () => {
    this.setState({alertVisible: false});
  }

  retourAccueil = () => {
    this.props.history.push('/');
  }

  render = () => (
    <Row>
      <Col sm={12}>
        <Row>
          <Col sm={12}>
            <Alert color={"danger"} isOpen={this.state.alertVisible} toggle={this.dismissAlert}>
              {this.state.alertMessage}
            </Alert>
          </Col>
        </Row>
        <Form onSubmit={this.send}>
          <FormGroup row>
            <Label for={'name'} sm={2}>Nom de l'animal :</Label>
            <Col sm={10}>
              <InputGroup>
                <InputGroupAddon addonType={'prepend'}>
                  <InputGroupText>
                    <FontAwesomeIcon icon={'tag'} />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type={'text'} name={'name'} id={'name'} placeholder={'Nom de l\'animal'}
                  value={this.state.name} valid={this.state.nameValide === 1} invalid={this.state.nameValide === 2} onChange={this.handleChange} onBlur={this.testChange} autoFocus />
                <FormFeedback className={'text-right'}>Le nom de l'animal doit comporter au minimum 3 caractères.</FormFeedback>
              </InputGroup>
            </Col>
          </FormGroup>
          <FormGroup row>
            <ButtonGroup>
              <Button color={'success'}><FontAwesomeIcon icon={'paw'}/> Adopter</Button>
              <Button color={'danger'} onClick={this.retourAccueil}><FontAwesomeIcon icon={'undo'}/> Retour à l'accueil</Button>
            </ButtonGroup>
          </FormGroup>
        </Form>
      </Col>
    </Row>
  );
}