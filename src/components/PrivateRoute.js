import React from 'react';
import API from '../utils/API';
import {Route, Redirect} from 'react-router-dom';

export const PrivateRoute = ({component: Component, ...rest}) => (
  <Route {...rest} render={(props) => {
      // const path = props.location.pathname;
      if(!API.isAuth())
      {
        return(<Redirect to={'/login'}/>);
      }
      else {
        return(<Component {...props} />);
      }
    }
  } />
);