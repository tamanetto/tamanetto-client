import React from 'react';
import { Button, Col, Row } from 'reactstrap'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import API from '../../utils/API';
import { PetContainer } from './PetContainer'
import { NavLink } from 'react-router-dom'

export class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {user: {pets: []}};
    API.userInfos().then(user => {
      console.log(user);
      this.setState({user});
    }).catch(error => {
      console.error(error.response.data.text)
      if(error.response.status === 403) {
        API.logout();
        this.props.history.push('/');
      }
    });
    this.createPet.bind(this);
    this.disconnect.bind(this);

    this.newUrl = '/pets/new';
  }

  disconnect = event => {
    this.props.history.push('/logout');
  }

  createPet = event => {
    this.props.history.push(this.newUrl);
  }

  render() {
    return(
      <Row className='dashboard'>
        <Col sm={12}>
          <h1>Bienvenue {this.state.user.username}</h1>
        </Col>
        <Col sm={12}>
          <Row className={"justify-content-between"}>
            <Col lg={6} sm={6}>
              <Button onClick={this.disconnect} size={'large'} color={'warning'}><FontAwesomeIcon icon={'sign-out-alt'}/> Déconnexion</Button>
            </Col>
            <Col sm={6} lg={6}>
              <Button onClick={this.createPet} size={'large'} color={'success'}><FontAwesomeIcon icon={'plus'}/> Nouvel animal</Button>
            </Col>
          </Row>
          <Row>
            <Col sm={12}>
              <Row>
                {this.state.user.pets.map((pet) => (<PetContainer pet={pet} key={pet._id} sm={2} />))}
              </Row>
            </Col>
          </Row>
          <Row>
            {this.state.user.pets.length === 0 && (<Col sm={12}>
              On dirait que vous n'avez pas d'animal ! Que diriez-vous d'en adopter un immédiatement ? <NavLink to={this.newUrl}>Adopter un animal</NavLink>
            </Col>)}
          </Row>
        </Col>
      </Row>
    )
  }
}