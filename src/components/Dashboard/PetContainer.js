import React from 'react';
import { Col, Row } from 'reactstrap'
import { NavLink } from 'react-router-dom'

export const PetContainer = ({pet, sm, ...props}) => (
  <Col sm={sm} className={"pet-container"}>
    <Row>
      <NavLink to={`/pets/view/${pet._id}`}>{pet.name}</NavLink>
    </Row>
  </Col>
);