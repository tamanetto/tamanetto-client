import React from 'react';
import { Button, InputGroup, Form, FormGroup, Input, InputGroupText, InputGroupAddon, Row, Col, Alert, ButtonGroup, Container } from 'reactstrap'
import API from '../../utils/API';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      usernameValide: 0,
      passwordValide: 0,
      alertVisible: false,
      alertMessage: '',
    }

    this.handleChange.bind(this);
    this.send.bind(this);
    this.testChange.bind(this);
    this.dismissAlert.bind(this);
    this.goInscription.bind(this);
  }

  send = (event) => {
    event.preventDefault();
    if(this.state.username.length === 0) {
      this.setState({
        usernameValide: 2,
        alertVisible: true,
        alertMessage: "Vous n'avez pas saisi de nom d'utilisateur.",
      })
      return;
    }
    if(this.state.password.length === 0) {
      this.setState({
        passwordValide: 2,
        alertVisible: true,
        alertMessage: "Vous n'avez pas saisi de mot de passe.",
      })
      return;
    }

    API.login(this.state.username, this.state.password).then((data) => {
      localStorage.setItem('token', data.data.token);
      // window.location = '/dashboard';
      this.props.history.push('/');
    }, (error) => {
      this.setState({
        alertVisible: true,
        alertMessage: error.response.data.text,
      })
      console.log(error);
      return;
    })
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    });

    if(this.state[event.target.id + "Valide"] === 2) {
      this.testChange(event);
    }
  };

  testChange = event => {
    if(this.state[event.target.id].length === 0) {
      this.setState({[event.target.id + "Valide"]: 2})
    }
    else {
      this.setState({[event.target.id + "Valide"]: 0});
    }
  };

  dismissAlert = () => {
    this.setState({alertVisible: false});
  }

  goInscription = () => {
    this.props.history.push('/signup');
  }

  render() {
    return(
      <Container>
        <Row>
          <Col sm={12}>
            <Row>
              <Col sm={12}>
                <Alert color={'danger'} isOpen={this.state.alertVisible} toggle={this.dismissAlert}>
                  {this.state.alertMessage}
                </Alert>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <Form onSubmit={this.send}>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend"><InputGroupText><FontAwesomeIcon icon="user"/></InputGroupText></InputGroupAddon>
                      <Input type="text" name="username" valid={this.state.usernameValide === 1} invalid={this.state.usernameValide === 2} onBlur={this.testChange} id="username" placeholder="Nom d'utilisateur" onChange={this.handleChange} value={this.state.username} autoFocus />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend"><InputGroupText><FontAwesomeIcon icon="key"/></InputGroupText></InputGroupAddon>
                    <Input type="password" name="password" valid={this.state.passwordValide === 1} invalid={this.state.passwordValide === 2} onBlur={this.testChange} id="password" placeholder="Mot de passe" onChange={this.handleChange} value={this.state.password} />
                  </InputGroup>
                  </FormGroup>
                  <FormGroup className={"justify-content-center"} row>
                    <ButtonGroup>
                      <Button color="success"><FontAwesomeIcon icon="sign-in-alt"/> Connexion</Button>
                      <Button color="primary" onClick={this.goInscription}><FontAwesomeIcon icon={'user-plus'}/> S'inscrire</Button>
                    </ButtonGroup>
                  </FormGroup>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }

}