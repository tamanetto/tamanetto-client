import React from 'react'
import API from '../utils/API';
import { NavLink } from 'react-router-dom'
import { Col, Row } from 'reactstrap'

export class Logout extends React.Component {
  constructor(props) {
    super(props);
    API.logout();
  }

  render = () => (<Row>
    <Col sm={12}>
      <p>Vous avez été correctement déconnecté. <NavLink to={'/'}>Retour à l'accueil</NavLink></p>
    </Col>
  </Row>)
}