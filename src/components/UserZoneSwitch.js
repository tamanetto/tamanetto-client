import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import {PrivateRoute} from './PrivateRoute'
import { Dashboard } from './Dashboard/Dashboard'
import { Container } from 'reactstrap'
import { PetsCreator } from './Pets/PetsCreator'
import { Logout } from './Logout'

export const UserZoneSwitch = () => (
  <Container>
    <Switch>
      <PrivateRoute path={"/pets/new"} component={PetsCreator}/>
      <PrivateRoute exact path={"/"} component={Dashboard} />
      <PrivateRoute exact path={"/logout"} component={Logout} />
      <Redirect push={true} to={'/'} />
    </Switch>
  </Container>
);